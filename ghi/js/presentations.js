window.addEventListener('DOMContentLoaded', async () => {
    // url is the one we used in insomnia/ declaring url for api
    const url = 'http://localhost:8000/api/conferences/';
    // fetch the url w await so we get response not promise
    const response = await fetch(url);
    // if response is ok, then get the data using .json method, await.
    if (response.ok) {
        const data = await response.json();
        console.log(data)
        const selectTag = document.getElementById('conference');
        for (let conference of data.conferences) {
            // Create an 'option' element
            const option = document.createElement('option');
            // Set the '.value' property of the option element to the
            // state's abbreviation
            option.value = conference.id;
            // Set the '.innerHTML' property of the option element to
            // the state's name
            option.innerHTML = conference.name;
            // Append the option element as a child of the select tag
            selectTag.appendChild(option);
        }
        const formTag = document.getElementById('create-presentations-form');
        formTag.addEventListener('submit', async event => {
            event.preventDefault();
            const selectTag = document.getElementById('conference')
            const formData = new FormData(formTag);
            const json = JSON.stringify(Object.fromEntries(formData));
            const conferenceData = JSON.parse(json)
            console.log(json)
            const locationUrl = `http://localhost:8000/api/conferences/${conferenceData.conference}/presentations/`;
            const fetchConfig = {
                method: "post",
                body: json,
                headers: {
                    'Content-Type': 'application/json',
                },
            };
            const response = await fetch(locationUrl, fetchConfig);
            if (response.ok) {
                formTag.reset();
                const presentation = await response.json();
                console.log(presentation);
            }
        });
    }
});